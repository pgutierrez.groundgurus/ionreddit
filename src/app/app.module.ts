import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SettingsPage } from "../pages/settings/settings";
import { RedditPage } from "../pages/reddit/reddit";
import { RedditPageModule } from "../pages/reddit/reddit.module";
import { SettingsPageModule } from "../pages/settings/settings.module";
import { AboutPageModule } from "../pages/about/about.module";
import { AboutPage } from "../pages/about/about";
import { DetailsPageModule } from "../pages/details/details.module";

@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    AboutPageModule,
    DetailsPageModule,
    RedditPageModule,
    SettingsPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    RedditPage,
    SettingsPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
