import { RedditPage } from '../reddit/reddit';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  category: string;
  limit: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.getDefaults();
  }

  getDefaults() {
    var category = localStorage.getItem('category');
    if (category != null) {
      this.category = category;
    } else {
      this.category = 'sports';
    }

    var limit = localStorage.getItem('limit');
    if (limit != null) {
      this.limit = parseInt(limit);
    } else {
      this.limit = 10;
    }
  }

  setDefaults() {
    localStorage.setItem('category', this.category);
    localStorage.setItem('limit', this.limit.toLocaleString());

    this.navCtrl.push(RedditPage);
  }
}
